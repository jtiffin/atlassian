#!/bin/bash
########################################################################################################################
#
#	Script Name: jira_backup_cleanup.sh
# Author: Jeff Tiffin
# Company: Signet Jewelers
# Date Created: 12/21/2016
# Date Finalized: 
# Reference: JFJ-2309	
# Purpose: This script will clean up the /var/atlassian/application-data/jira/export directory on the given server.
#					 Any files over 45 days old will be purged from the directory.
#
########################################################################################################################